package com.gabriel.alauk.test.project.infra.util;

import com.gabriel.alauk.test.project.config.AbstractJUnitTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.BDDAssertions.then;

@DisplayName("Junit Tests for Decimal Utils")
class DecimalUtilsTest extends AbstractJUnitTest {

    @Test
    @DisplayName("get a valid Value Two Decimal Rounded Half Up")
    void get_a_valid_Value_Two_Decimal_Rounded_Half_Up() {
        double valueTwoDecimalRoundedHalfUp = DecimalUtils.getValueTwoDecimalRoundedHalfUp(1234.1298);

        then(valueTwoDecimalRoundedHalfUp).isEqualTo(1234.13);

        then(valueTwoDecimalRoundedHalfUp).isNotEqualTo(1234.12);
    }
}
