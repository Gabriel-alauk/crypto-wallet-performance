package com.gabriel.alauk.test.project.config.fixture;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetResponseVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetVO;

public class AssetVOFixture implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(AssetVO.class)
                .addTemplate("BTC", new Rule() {{
                    add("id", "bitcoin");
                    add("symbol", "BTC");
                }})
                .addTemplate("ETH", new Rule() {{
                    add("id", "ethereum");
                    add("symbol", "ETH");
                }})
                .addTemplate("USDT", new Rule() {{
                    add("id", "tether");
                    add("symbol", "USDT");
                }})
                .addTemplate("empty", new Rule() {});
    }
}
