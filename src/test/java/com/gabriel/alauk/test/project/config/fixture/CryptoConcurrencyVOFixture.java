package com.gabriel.alauk.test.project.config.fixture;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.gabriel.alauk.test.project.domain.vo.file.CryptoConcurrencyVO;

public class CryptoConcurrencyVOFixture implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(CryptoConcurrencyVO.class)
                .addTemplate("BTC", new Rule() {{
                    add("symbol", "BTC");
                    add("quantity", 0.12345);
                    add("price", 37870.5058);
                }})
                .addTemplate("ETH", new Rule() {{
                    add("symbol", "ETH");
                    add("quantity", 4.89532);
                    add("price", 2004.9774);
                }})
                .addTemplate("USDT", new Rule() {{
                    add("symbol", "USDT");
                    add("quantity", 2.4563);
                    add("price", 1.000426);
                }})
                .addTemplate("empty", new Rule() {});
    }
}
