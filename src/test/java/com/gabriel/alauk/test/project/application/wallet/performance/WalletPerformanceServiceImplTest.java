package com.gabriel.alauk.test.project.application.wallet.performance;

import br.com.six2six.fixturefactory.Fixture;
import com.gabriel.alauk.test.project.application.wallet.WalletService;
import com.gabriel.alauk.test.project.config.AbstractJUnitTest;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetResponseVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryDataVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryResponseVO;
import com.gabriel.alauk.test.project.domain.vo.file.CryptoConcurrencyVO;
import com.gabriel.alauk.test.project.domain.vo.file.WalletVO;
import com.gabriel.alauk.test.project.domain.vo.output.ResultDataVO;
import com.gabriel.alauk.test.project.infra.external.CoinCapRestClient;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.BDDMockito.given;

@DisplayName("JUnit tests of Wallet Performance")
class WalletPerformanceServiceImplTest extends AbstractJUnitTest {

    @InjectMocks
    WalletPerformanceServiceImpl walletPerformanceService;

    @Mock
    CoinCapRestClient coinCapRestClient;

    @Mock
    WalletService walletService;

    @Test
    @DisplayName("process wallet performance will return a valid result data")
    void process_wallet_performance_will_return_valid_result_data() {
        List<String> assets = Arrays.asList("BTC", "ETH", "USDT");
        List<CryptoConcurrencyVO> cryptoConcurrencyVOS = new ArrayList<>();
        for (String asset : assets) {
            cryptoConcurrencyVOS.add(Fixture.from(CryptoConcurrencyVO.class).gimme(asset));
        }
        WalletVO walletVO = new WalletVO(cryptoConcurrencyVOS);
        given(walletService.getWalletByResources()).willReturn(walletVO);
        ResultDataVO expected = new ResultDataVO();
        expected.incrementToTotal(17938.739684959648);
        expected.setBestPerformance(388.27, "USDT");
        expected.setWorstPerformance(1.01, "ETH");

        for (CryptoConcurrencyVO cryptoConcurrencyVO : cryptoConcurrencyVOS) {
            AssetRequestVO assetRequestVO = new AssetRequestVO(cryptoConcurrencyVO.getSymbol(), 10L, 0L);
            AssetResponseVO assetResponseVO = new AssetResponseVO();
            AssetVO assetVO = Fixture.from(AssetVO.class).gimme(cryptoConcurrencyVO.getSymbol());
            assetResponseVO.setData(List.of(assetVO));
            given(coinCapRestClient.getAssets(refEq(assetRequestVO))).willReturn(assetResponseVO);

            HistoryRequestVO historyRequestVO = new HistoryRequestVO(assetVO.getId(),
                    "d1",
                    1617753600000L,
                    1617753601000L);
            HistoryResponseVO inputHistory = new HistoryResponseVO();
            HistoryDataVO historyDataVO = Fixture.from(HistoryDataVO.class).gimme(cryptoConcurrencyVO.getSymbol());
            inputHistory.setData(List.of(historyDataVO));
            given(coinCapRestClient.getHistory(refEq(historyRequestVO))).willReturn(inputHistory);
        }

        walletPerformanceService.processWalletPerformance();

        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(walletPerformanceService.getResult())
                    .as("actual should be equal to expected")
                    .usingRecursiveComparison()
                    .withStrictTypeChecking()
                    .isEqualTo(expected);
        });
    }
}
