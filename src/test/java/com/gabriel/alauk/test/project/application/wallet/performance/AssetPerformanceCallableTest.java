package com.gabriel.alauk.test.project.application.wallet.performance;

import br.com.six2six.fixturefactory.Fixture;
import com.gabriel.alauk.test.project.config.AbstractJUnitTest;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetResponseVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryDataVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryResponseVO;
import com.gabriel.alauk.test.project.domain.vo.file.CryptoConcurrencyVO;
import com.gabriel.alauk.test.project.domain.vo.output.AssetInfoVO;
import com.gabriel.alauk.test.project.domain.vo.output.ResultDataVO;
import com.gabriel.alauk.test.project.exception.WalletPerformanceServiceException;
import com.gabriel.alauk.test.project.infra.external.CoinCapRestClient;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.BDDMockito.given;

@DisplayName("JUnit test of Asset Performance Callable")
class AssetPerformanceCallableTest extends AbstractJUnitTest {

    AssetPerformanceCallable assetPerformanceCallable;

    @Mock
    CoinCapRestClient coinCapRestClient;

    CryptoConcurrencyVO cryptoConcurrencyVO;

    ResultDataVO resultDataVO = new ResultDataVO();

    AssetResponseVO assetResponseVO = new AssetResponseVO();

    @BeforeEach
    void beforeEach() {
        List<String> assets = Arrays.asList("BTC", "ETH", "USDT");
        int round = (int) Math.ceil(Math.random() * 3 - 1);
        cryptoConcurrencyVO = Fixture.from(CryptoConcurrencyVO.class).gimme(assets.get(round));

        assetPerformanceCallable = new AssetPerformanceCallable(cryptoConcurrencyVO, resultDataVO, coinCapRestClient);
    }

    @Nested
    @DisplayName("Valid JUnit Tests")
    class ValidAssetPerformanceCallableTest {

        AssetRequestVO assetRequestVO;
        AssetVO assetVO;

        @BeforeEach
        void beforeEach() {
            assetVO = Fixture.from(AssetVO.class).gimme(cryptoConcurrencyVO.getSymbol());
            assetResponseVO.setData(List.of(assetVO));

            assetRequestVO = new AssetRequestVO(cryptoConcurrencyVO.getSymbol(), 10L, 0L);

            given(coinCapRestClient.getAssets(refEq(assetRequestVO))).willReturn(assetResponseVO);
        }

        @Test
        @DisplayName("call method will return a asset info valid")
        void call_method_will_return_a_asset_info_valid() {

            HistoryRequestVO historyRequestVO = new HistoryRequestVO(assetVO.getId(),
                    "d1",
                    1617753600000L,
                    1617753601000L);
            HistoryResponseVO inputHistory = new HistoryResponseVO();
            HistoryDataVO historyDataVO = Fixture.from(HistoryDataVO.class).gimme(cryptoConcurrencyVO.getSymbol());
            inputHistory.setData(List.of(historyDataVO));
            given(coinCapRestClient.getHistory(refEq(historyRequestVO))).willReturn(inputHistory);

            AssetInfoVO expected = new AssetInfoVO(
                    assetVO.getId(),
                    assetVO.getSymbol(),
                    cryptoConcurrencyVO.getQuantity(),
                    cryptoConcurrencyVO.getPrice(),
                    historyDataVO.getPriceUsd());

            AssetInfoVO actual;
            try {
                actual = assetPerformanceCallable.call();
            } catch (InterruptedException e) {
                throw new RuntimeException("Error on test. Can't be able to get return from the thread.");
            }
            SoftAssertions.assertSoftly(softAssertions -> {
                softAssertions.assertThat(actual)
                        .as("actual should be equal expected")
                        .usingRecursiveComparison()
                        .withStrictTypeChecking()
                        .isEqualTo(expected);
            });

        }

        @Test
        @DisplayName("getAsset method will return a AssetVO valid")
        void get_asset_method_will_return_a_asset_vo_valid() {
            AssetVO expected = Fixture.from(AssetVO.class).gimme(cryptoConcurrencyVO.getSymbol());

            AssetVO actual = assetPerformanceCallable.getAsset(assetRequestVO);

            SoftAssertions.assertSoftly(softAssertions -> {
                softAssertions.assertThat(actual)
                        .as("actual should be equal expected")
                        .usingRecursiveComparison()
                        .withStrictTypeChecking()
                        .isEqualTo(expected);
            });
        }
    }



    @Test
    @DisplayName("getAsset method will throw a exception if event asset won't found")
    void get_asset_method_will_throw_a_exception_if_even_asset_wont_found() {
        assetResponseVO.setData(new ArrayList<>());

        AssetRequestVO assetRequestVO = new AssetRequestVO(cryptoConcurrencyVO.getSymbol(), 10L, 0L);
        given(coinCapRestClient.getAssets(assetRequestVO)).willReturn(assetResponseVO);

        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThatThrownBy(() -> assetPerformanceCallable.getAsset(assetRequestVO))
                    .as("actual should throwing a exception")
                    .hasMessage("Asset not founded.")
                    .as("Check if it's the exception correct")
                    .isInstanceOf(WalletPerformanceServiceException.class);
        });
    }
}
