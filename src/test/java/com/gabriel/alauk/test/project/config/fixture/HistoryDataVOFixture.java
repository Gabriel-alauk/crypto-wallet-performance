package com.gabriel.alauk.test.project.config.fixture;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryDataVO;

import java.util.Date;

public class HistoryDataVOFixture implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(HistoryDataVO.class)
                .addTemplate("BTC", new Rule() {{
                    add("priceUsd", "56999.9728252053067291");
                    add("time", new Date().getTime());
                    add("date", new Date());
                }})
                .addTemplate("ETH").inherits("BTC", new Rule() {{
                    add("priceUsd", "2032.1394325557042107");
                }})
                .addTemplate("USDT").inherits("BTC", new Rule() {{
                    add("priceUsd", "388.4379891338453617");
                }})
                .addTemplate("empty", new Rule() {});
    }
}
