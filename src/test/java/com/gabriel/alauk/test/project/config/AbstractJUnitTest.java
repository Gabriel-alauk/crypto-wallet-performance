package com.gabriel.alauk.test.project.config;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static br.com.six2six.fixturefactory.loader.FixtureFactoryLoader.loadTemplates;

@ExtendWith(MockitoExtension.class)
public abstract class AbstractJUnitTest {

    @BeforeAll
    public static void setUp() {
        loadTemplates("com.gabriel.alauk.test.project");
    }

}
