package com.gabriel.alauk.test.project.infra.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DecimalUtils {

    public static double getValueTwoDecimalRoundedHalfUp(double value) {
        return BigDecimal.valueOf(value).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
    }
}
