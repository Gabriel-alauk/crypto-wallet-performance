package com.gabriel.alauk.test.project.application.wallet.performance;

import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetResponseVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryDataVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryResponseVO;
import com.gabriel.alauk.test.project.domain.vo.file.CryptoConcurrencyVO;
import com.gabriel.alauk.test.project.domain.vo.output.AssetInfoVO;
import com.gabriel.alauk.test.project.domain.vo.output.ResultDataVO;
import com.gabriel.alauk.test.project.exception.WalletPerformanceServiceException;
import com.gabriel.alauk.test.project.infra.external.CoinCapRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

class AssetPerformanceCallable implements Callable<AssetInfoVO> {

    private final CryptoConcurrencyVO cryptoCoin;
    private final ResultDataVO resultDataVO;
    private final CoinCapRestClient coinCapRestClient;
    private static final Logger LOGGER = LoggerFactory.getLogger(AssetPerformanceCallable.class);
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public AssetPerformanceCallable(final CryptoConcurrencyVO cryptoCoin,
                                    final ResultDataVO resultDataVO,
                                    final CoinCapRestClient coinCapRestClient) {
        this.cryptoCoin = cryptoCoin;
        this.resultDataVO = resultDataVO;
        this.coinCapRestClient = coinCapRestClient;
    }

    public AssetInfoVO call() throws InterruptedException {

        LOGGER.info("Submitted request {} at {}", cryptoCoin.getSymbol(), simpleDateFormat.format(new Date()));
        AssetVO assetVO = getAsset(new AssetRequestVO(cryptoCoin.getSymbol(), 10L, 0L));

        HistoryResponseVO history = coinCapRestClient.getHistory(new HistoryRequestVO(assetVO.getId(),
                "d1",
                1617753600000L,
                1617753601000L));

        if (!history.getData().isEmpty()) {
            HistoryDataVO actualValue = history.getData().get(0);
            AssetInfoVO assetInfoVO = new AssetInfoVO(assetVO.getId(),
                    assetVO.getSymbol(),
                    cryptoCoin.getQuantity(),
                    cryptoCoin.getPrice(),
                    actualValue.getPriceUsd());
            resultDataVO.incrementToTotal(assetInfoVO.getValueUsd());
            resultDataVO.setBestPerformance(assetInfoVO.getPerformance(), assetInfoVO.getSymbol());
            resultDataVO.setWorstPerformance(assetInfoVO.getPerformance(), assetInfoVO.getSymbol());
            return assetInfoVO;
        }
        return null;
    }

    protected AssetVO getAsset(AssetRequestVO assetRequestVO) {
        AssetResponseVO assets = coinCapRestClient.getAssets(assetRequestVO);
        return assets
                .getData().stream()
                .filter(d -> d.getSymbol()
                        .equals(cryptoCoin.getSymbol()))
                .findFirst()
                .orElseThrow(() -> {throw new WalletPerformanceServiceException("Asset not founded.");});
    }
}
