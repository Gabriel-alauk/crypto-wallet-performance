package com.gabriel.alauk.test.project.domain.vo.output;

import com.gabriel.alauk.test.project.infra.util.AtomicDouble;
import com.gabriel.alauk.test.project.infra.util.DecimalUtils;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicReference;

public class ResultDataVO implements Serializable {
    private final AtomicDouble total;
    private final AtomicDouble bestPerformance;
    private final AtomicReference<String> bestAsset;
    private final AtomicDouble worstPerformance;
    private final AtomicReference<String> worstAsset;

    public ResultDataVO() {
        total = new AtomicDouble();
        bestPerformance = new AtomicDouble();
        worstPerformance = new AtomicDouble();
        bestAsset = new AtomicReference<>();
        worstAsset = new AtomicReference<>();
    }

    public double getTotal() {
        return total.get();
    }

    public void incrementToTotal(Double increment) {
        boolean incremented;
        do {
            incremented = total.compareAndSet(getTotal(), increment+getTotal());
        } while (!incremented);
    }

    public double getBestPerformance() {
        return bestPerformance.get();
    }

    public void setBestPerformance(Double newValue, String bestAsset) {
        boolean updated = false;
        do {
            double actual = getBestPerformance();
            if (newValue > actual) {
                updated = this.bestPerformance.compareAndSet(actual, newValue);
                if (updated) setBestAsset(newValue, bestAsset);
            } else break;
        } while (!updated);
    }

    public void setBestAsset(Double newValue, String bestAsset) {
        boolean updated = false;
        do {
            String actual = getBestAsset();
            if (newValue == getBestPerformance()) {
                updated = this.bestAsset.compareAndSet(actual, bestAsset);
            } else break;
        } while (!updated);
    }

    private String getBestAsset() {
        return bestAsset.get();
    }

    private String getWorstAsset() {
        return worstAsset.get();
    }

    public double getWorstPerformance() {
        return worstPerformance.get();
    }

    public void setWorstPerformance(Double newValue, String worstAsset) {
        boolean updated = false;
        boolean init;
        do {
            double actual = getWorstPerformance();
            init = actual == 0;
            if (newValue < actual || init) {
                updated = this.worstPerformance.compareAndSet(actual, newValue);
                if (updated) {
                    this.setWorstAsset(newValue, worstAsset);
                }
            } else break;
        } while (!init && !updated);

    }

    public void setWorstAsset(Double newValue, String worstAsset) {
        boolean update = false;
        do {
            String actualWorst = getWorstAsset();
            if (newValue == getWorstPerformance()) {
                update = this.worstAsset.compareAndSet(actualWorst, worstAsset);
            } else break;
        } while (!update);
    }

    public Double getTotalFormatted() {
        return DecimalUtils.getValueTwoDecimalRoundedHalfUp(getTotal());
    }

    public Double getBestPerformanceFormatted() {
        return DecimalUtils.getValueTwoDecimalRoundedHalfUp(getBestPerformance());
    }

    public Double getWorstPerformanceFormatted() {
        return DecimalUtils.getValueTwoDecimalRoundedHalfUp(getWorstPerformance());
    }

    @Override
    public String toString() {
        return "{" +
                "total=" + getTotalFormatted() + "\n" +
                ", best_performance=" + getBestPerformanceFormatted() + "\n" +
                ", best_asset=" + bestAsset + "\n" +
                ", worst_performance=" + getWorstPerformanceFormatted() + "\n" +
                ", worst_asset=" + worstAsset + "\n" +
                '}';
    }


}
