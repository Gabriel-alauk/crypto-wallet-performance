package com.gabriel.alauk.test.project.domain.vo.coincap.asset;

import java.io.Serializable;
import java.util.List;

public class AssetResponseVO implements Serializable {
    private List<AssetVO> data;
    private Long timestamp;

    public List<AssetVO> getData() {
        return data;
    }

    public void setData(List<AssetVO> data) {
        this.data = data;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "AssetResponseVO{" +
                "data=" + data.toString() +
                ", timestamp=" + timestamp +
                '}';
    }
}
