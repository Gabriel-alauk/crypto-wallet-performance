package com.gabriel.alauk.test.project.domain.vo.coincap.asset;

import java.io.Serializable;

public class AssetRequestVO implements Serializable {

    private final String search;
    private final Long limit;
    private final Long offset;

    public AssetRequestVO(String search, Long limit, Long offset) {
        this.search = search;
        this.limit = limit;
        this.offset = offset;
    }

    public String getSearch() {
        return search;
    }

    public Long getLimit() {
        return limit;
    }

    public Long getOffset() {
        return offset;
    }
}
