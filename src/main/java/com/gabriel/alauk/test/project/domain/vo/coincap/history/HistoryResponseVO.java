package com.gabriel.alauk.test.project.domain.vo.coincap.history;

import java.io.Serializable;
import java.util.List;

public class HistoryResponseVO implements Serializable {
    private List<HistoryDataVO> data;
    private Long timestamp;

    public List<HistoryDataVO> getData() {
        return data;
    }

    public void setData(List<HistoryDataVO> data) {
        this.data = data;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "HistoryResponseVO{" +
                "data=" + data.toString() +
                ", timestamp=" + timestamp +
                '}';
    }
}
