package com.gabriel.alauk.test.project.application.wallet;

import com.gabriel.alauk.test.project.domain.vo.file.WalletVO;

public interface WalletService {
    WalletVO getWalletByResources();
}
