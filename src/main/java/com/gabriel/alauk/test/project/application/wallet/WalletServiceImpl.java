package com.gabriel.alauk.test.project.application.wallet;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.gabriel.alauk.test.project.domain.vo.file.CryptoConcurrencyVO;
import com.gabriel.alauk.test.project.domain.vo.file.WalletVO;
import com.gabriel.alauk.test.project.exception.WalletServiceException;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
class WalletServiceImpl implements WalletService {

    private final CsvMapper csvMapper;

    WalletServiceImpl() {
        this.csvMapper = new CsvMapper();
    }

    @Override
    public WalletVO getWalletByResources() {
        InputStream resourceAsStream = WalletServiceImpl.class
                .getClassLoader()
                .getResourceAsStream("csv/wallet.csv");
        String csvString = getFileAsStringByResources(resourceAsStream);
        assert csvString != null;

        List<CryptoConcurrencyVO> cryptoCoinList = getCryptoCoinList(csvString);
        assert cryptoCoinList != null && !cryptoCoinList.isEmpty();

        return new WalletVO(cryptoCoinList);
    }

    private List<CryptoConcurrencyVO> getCryptoCoinList(String csvString) {
        try {
            CsvSchema headerSchema = CsvSchema.emptySchema().withHeader();
            MappingIterator<CryptoConcurrencyVO> objectMappingIterator = csvMapper
                    .readerFor(CryptoConcurrencyVO.class)
                    .with(headerSchema)
                    .readValues(csvString);
            return objectMappingIterator.readAll();
        } catch (Exception e) {
            throw new WalletServiceException("Occurring an error during mapping the wallet from string to object", e);
        }
    }

    private String getFileAsStringByResources(InputStream resourceAsStream) {
        String csvString;
        try {
            csvString = IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new WalletServiceException("Occurring an error during searching for wallet.csv file", e);
        }
        return csvString;
    }
}
