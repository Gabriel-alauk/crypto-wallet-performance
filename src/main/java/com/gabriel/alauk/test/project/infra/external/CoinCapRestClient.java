package com.gabriel.alauk.test.project.infra.external;

import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetResponseVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryResponseVO;

public interface CoinCapRestClient {

    AssetResponseVO getAssets(AssetRequestVO request);

    HistoryResponseVO getHistory(HistoryRequestVO request);
}
