package com.gabriel.alauk.test.project.application.wallet.performance;

import com.gabriel.alauk.test.project.application.wallet.WalletService;
import com.gabriel.alauk.test.project.domain.vo.file.CryptoConcurrencyVO;
import com.gabriel.alauk.test.project.domain.vo.file.WalletVO;
import com.gabriel.alauk.test.project.domain.vo.output.AssetInfoVO;
import com.gabriel.alauk.test.project.domain.vo.output.ResultDataVO;
import com.gabriel.alauk.test.project.exception.WalletPerformanceServiceException;
import com.gabriel.alauk.test.project.infra.external.CoinCapRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
class WalletPerformanceServiceImpl implements WalletPerformanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WalletPerformanceServiceImpl.class);
    private final WalletService walletService;
    private final CoinCapRestClient coinCapRestClient;
    private final ResultDataVO result;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private ExecutorService executorService;
    private ExecutorCompletionService<AssetInfoVO> completionService;

    WalletPerformanceServiceImpl(final WalletService walletService,
                                 final CoinCapRestClient coinCapRestClient) {
        this.walletService = walletService;
        this.coinCapRestClient = coinCapRestClient;
        this.result = new ResultDataVO();
    }

    @Override
    public void processWalletPerformance() {
        WalletVO walletByResources = walletService.getWalletByResources();
        LOGGER.info("Now is {}", simpleDateFormat.format(new Date()));
        if (!walletByResources.getCryptoCoins().isEmpty()) {
            executorService = Executors.newFixedThreadPool(3);
            completionService = new ExecutorCompletionService<>(executorService);

            this.awaitTerminationThreads(executorService);
            for (CryptoConcurrencyVO cryptoCoin : walletByResources.getCryptoCoins()) {
                AssetPerformanceCallable assetPerformanceCallable =
                        new AssetPerformanceCallable(cryptoCoin, result, coinCapRestClient);
                completionService.submit(assetPerformanceCallable);
            }

            this.getValuesAndCalculateResults(walletByResources.getCryptoCoins().size(), completionService);
            executorService.shutdownNow();
            LOGGER.info(result.toString());
        } else {
            LOGGER.warn("There aren't assets in wallet.csv.");
        }
    }

    private void getValuesAndCalculateResults(int listSize,
                                              ExecutorCompletionService<AssetInfoVO> completionService) {
        for (int i = 0; i < listSize; i++) {
            try {
                completionService.take().get();
            } catch (InterruptedException | ExecutionException e) {
                executorService.shutdownNow();
                throw new WalletPerformanceServiceException("Occurring a error during getting value from thread. ", e);
            }
        }
    }

    private void awaitTerminationThreads(ExecutorService executorService) {
        ScheduledExecutorService executorAwaitTermination = Executors
                .newSingleThreadScheduledExecutor();
        executorAwaitTermination.schedule(() -> {
            if (!executorService.isTerminated()) {
                LOGGER.warn("(program hangs, waiting for some of the previous requests to finish)");
                this.awaitTerminationThreads(executorService);
            }
            executorAwaitTermination.shutdown();
        }, 10, TimeUnit.SECONDS);
    }

    protected ResultDataVO getResult() {
        return result;
    }

    protected ExecutorCompletionService<AssetInfoVO> getCompletionService() {
        return completionService;
    }
}
