package com.gabriel.alauk.test.project.config;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Configuration
public class OkHttpClientConfig {

    @Bean
    public OkHttpClient okHttpClient(){
        return new OkHttpClient.Builder()
                               .readTimeout(5, TimeUnit.SECONDS )
                               .addInterceptor( chain -> chain.proceed( chain.request()
                                                                             .newBuilder()
                                                                             .header("Accept", APPLICATION_JSON_VALUE)
                                                                             .addHeader("Content-Type", APPLICATION_JSON_VALUE )
                                                                             .build())).build();
    }

    public static RequestBody createPostRequestBodyApplicationJson(final String postContent ){
        return RequestBody.create( MediaType.parse( APPLICATION_JSON_VALUE ), postContent );
    }
}
