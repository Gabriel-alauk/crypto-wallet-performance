package com.gabriel.alauk.test.project.domain.vo.coincap.history;

import java.io.Serializable;

public class HistoryRequestVO implements Serializable {
    private final String id;
    private final String interval;
    private final Long start;
    private final Long end;

    public HistoryRequestVO(String id, String interval, Long start, Long end) {
        this.id = id;
        this.interval = interval;
        this.start = start;
        this.end = end;
    }

    public String getId() {
        return id;
    }

    public String getInterval() {
        return interval;
    }

    public Long getStart() {
        return start;
    }

    public Long getEnd() {
        return end;
    }
}
