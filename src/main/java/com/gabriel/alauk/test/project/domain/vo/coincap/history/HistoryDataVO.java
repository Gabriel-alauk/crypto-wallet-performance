package com.gabriel.alauk.test.project.domain.vo.coincap.history;

import java.io.Serializable;
import java.util.Date;

public class HistoryDataVO implements Serializable {

    private String priceUsd;
    private Long time;
    private Date date;

    public Double getPriceUsd() {
        return Double.parseDouble(priceUsd);
    }

    public void setPriceUsd(String priceUsd) {
        this.priceUsd = priceUsd;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "HistoryDataVO{" +
                "priceUsd='" + priceUsd + '\'' +
                ", time=" + time +
                ", date=" + date +
                '}';
    }
}
