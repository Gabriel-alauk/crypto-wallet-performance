package com.gabriel.alauk.test.project.domain.vo.output;

import com.gabriel.alauk.test.project.infra.util.DecimalUtils;

import java.io.Serializable;

public class AssetInfoVO implements Serializable {
    private final String id;
    private final String symbol;
    private final Double quantity;
    private final Double oldPriceUsd;
    private final Double actualPriceUsd;

    public AssetInfoVO(String id, String symbol, Double quantity, Double oldPriceUsd, Double actualPriceUsd) {
        this.id = id;
        this.symbol = symbol;
        this.quantity = quantity;
        this.oldPriceUsd = oldPriceUsd;
        this.actualPriceUsd = actualPriceUsd;
    }

    public String getId() {
        return id;
    }

    public String getSymbol() {
        return symbol;
    }

    public Double getQuantity() {
        return quantity;
    }

    public Double getOldPriceUsd() {
        return oldPriceUsd;
    }

    public Double getActualPriceUsd() {
        return actualPriceUsd;
    }

    public Double getValueUsd() {
        return actualPriceUsd * quantity;
    }

    public Double getPerformance() {
        return DecimalUtils.getValueTwoDecimalRoundedHalfUp(actualPriceUsd / oldPriceUsd);
    }
}
