package com.gabriel.alauk.test.project.domain.vo.file;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.Objects;

@JsonPropertyOrder({"symbol","quantity","price"})
public class CryptoConcurrencyVO implements Serializable {
    private final String symbol;
    private final Double quantity;
    private final Double price;

    public CryptoConcurrencyVO() {
        symbol = null;
        quantity = null;
        price = null;
    }

    public CryptoConcurrencyVO(String symbol, Double quantity, Double price) {
        this.symbol = symbol;
        this.quantity = quantity;
        this.price = price;
    }

    public String getSymbol() {
        return symbol;
    }

    public Double getQuantity() {
        return quantity;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CryptoConcurrencyVO that = (CryptoConcurrencyVO) o;
        return Objects.equals(symbol, that.symbol)
                && Objects.equals(quantity, that.quantity)
                && Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, quantity, price);
    }

    @Override
    public String toString() {
        return "CryptoCoin{" +
                "symbol='" + symbol + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
