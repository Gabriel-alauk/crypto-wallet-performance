package com.gabriel.alauk.test.project.infra.external;

import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.asset.AssetResponseVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryRequestVO;
import com.gabriel.alauk.test.project.domain.vo.coincap.history.HistoryResponseVO;
import com.gabriel.alauk.test.project.exception.CoinCapRestClientException;
import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Objects;

@Component
class CoinCapRestClientImpl implements CoinCapRestClient {

    private final OkHttpClient okHttpClient;
    private static final String JSON = "application/json";
    private final Gson gson = new Gson();

    @Value("${coincap.base-url}")
    private String baseUrl;

    CoinCapRestClientImpl(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    @Override
    public AssetResponseVO getAssets(AssetRequestVO request) {
        final Request requestBuilder = new Request.Builder()
                .get()
                .url(baseUrl
                        .concat("/v2/assets?")
                        .concat("search=" + request.getSearch())
                        .concat("&limit=" + request.getLimit())
                        .concat("&offset=" + request.getOffset())
                )
                .build();

        try(final Response response = okHttpClient.newCall(requestBuilder).execute()) {
            if (response.isSuccessful() && Objects.nonNull(response.body())) {
                return gson.fromJson(getBody(response), AssetResponseVO.class);
            }
            throw new CoinCapRestClientException(
                    "Occurring an error during calling of CoinCap API to get the assets info," +
                    " response status: " + response.code() +
                    " response body: " + getBody(response));
        } catch (CoinCapRestClientException e) {
            throw e;
        } catch (Exception e) {
            throw new CoinCapRestClientException("Occurring an error during calling of CoinCap API to get the assets info.", e);
        }
    }

    @Override
    public HistoryResponseVO getHistory(HistoryRequestVO request) {
        final Request requestBuilder = new Request.Builder()
                .get()
                .url(MessageFormat.format(baseUrl + "/v2/assets/{0}/history?interval={1}&start={2}&end={3}",
                        request.getId(),
                        request.getInterval(),
                        request.getStart().toString(),
                        request.getEnd().toString())
                )
                .build();

        try(final Response response = okHttpClient.newCall(requestBuilder).execute()) {
            if (response.isSuccessful() && Objects.nonNull(response.body())) {
                return gson.fromJson(getBody(response), HistoryResponseVO.class);
            }
            throw new CoinCapRestClientException(
                    "Occurring an error during calling of CoinCap API to get the history info," +
                            " response status: " + response.code() +
                            " response body: " + getBody(response));
        } catch (CoinCapRestClientException e) {
            throw e;
        } catch (Exception e) {
            throw new CoinCapRestClientException("Occurring an error during calling of CoinCap API to get the history info.", e);
        }
    }

    private String getBody(Response response) {
        try {
            return Objects.nonNull(response.body()) ? response.body().string() : "";
        } catch (Exception e) {
            return "";
        }
    }
}
