package com.gabriel.alauk.test.project.exception;

public class WalletPerformanceServiceException extends RuntimeException {

    public WalletPerformanceServiceException(String message) {
        super(message);
    }

    public WalletPerformanceServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
