package com.gabriel.alauk.test.project.application.wallet.performance;

public interface WalletPerformanceService {

    void processWalletPerformance();
}
