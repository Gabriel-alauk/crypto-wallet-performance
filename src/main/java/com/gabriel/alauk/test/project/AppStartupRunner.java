package com.gabriel.alauk.test.project;

import com.gabriel.alauk.test.project.application.wallet.performance.WalletPerformanceService;
import com.gabriel.alauk.test.project.domain.vo.output.ResultDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AppStartupRunner implements CommandLineRunner {
    private final WalletPerformanceService walletPerformanceService;
    private static final Logger LOGGER = LoggerFactory.getLogger(AppStartupRunner.class);

    public AppStartupRunner(WalletPerformanceService walletPerformanceService) {
        this.walletPerformanceService = walletPerformanceService;
    }

    @Override
    public void run(String...args) {
        walletPerformanceService.processWalletPerformance();
    }
}
