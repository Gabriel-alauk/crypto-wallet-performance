package com.gabriel.alauk.test.project.domain.vo.file;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class WalletVO implements Serializable {

    private final List<CryptoConcurrencyVO> cryptoCoins;

    public WalletVO(List<CryptoConcurrencyVO> cryptoCoins) {
        this.cryptoCoins = cryptoCoins;
    }

    public List<CryptoConcurrencyVO> getCryptoCoins() {
        return cryptoCoins;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WalletVO wallet = (WalletVO) o;
        return Objects.equals(cryptoCoins, wallet.cryptoCoins);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cryptoCoins);
    }

    @Override
    public String toString() {
        return "Wallet{" +
                "cryptoCoins=" + cryptoCoins +
                '}';
    }
}
