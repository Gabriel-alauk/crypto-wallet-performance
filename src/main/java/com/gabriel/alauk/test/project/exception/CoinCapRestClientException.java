package com.gabriel.alauk.test.project.exception;

public class CoinCapRestClientException extends RuntimeException {

    public CoinCapRestClientException(String message) {
        super(message);
    }

    public CoinCapRestClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
