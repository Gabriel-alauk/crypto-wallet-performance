# README #

### Quick summary  ###

* This project has objective to give a collection of crypto assets with their positions,
  their latest prices (from the CoinCap API)
  and return the updated total financial value of the wallet with performance data.

### Requirements ###

* Java 11+
* Maven (If even you use Windows, set up the maven configuration on PATH of your Windows user or admin user)
* Docker

### Running ###

* Move your wallet CSV to src/main/resources/csv/wallet.csv and replace the file existent. (Considering the file as charset UTF-8 format)
* Running in terminal: mvn spring-boot:run

